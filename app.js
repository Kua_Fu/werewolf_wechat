//app.js
var Util = require( 'utils/util.js' );
App({
  //app中的全局变量
  globalData:{
    // localhost: 'https://sjmemory.cn',
    // localhost: 'http://192.168.2.156:5058/',
    localhost: 'https://thewind.xin/werewolf/',
    header: {'Content-Type': 'application/x-www-form-urlencoded'},
  },

  requestRemote: function(urlInfo, requestInfo, cb){
    var that = this
    var queryInfoString = JSON.stringify(requestInfo)
    wx.request({
      // url: that.globalData.localhost + '/tender/' + urlInfo,
      url: that.globalData.localhost + urlInfo,
      method: 'post',
      header: that.globalData.header,
      data: Util.json2Form({data: queryInfoString}),
      success: function(res){
        cb(res.data)
      },
      fail: function(res){
      }
    })
  },

  requestRemoteWithData: function(urlInfo, requestInfo, cb){
    var that = this
    wx.request({
      url: that.globalData.localhost + urlInfo,
      method: 'post',
      header: that.globalData.header,
      data: Util.json2Form(requestInfo),
      success: function(res){
        cb(res.data)
      }
    })
  },

  //使用微信登录
  wechatLogin: function(cb, requestInfo){
    var that = this
    that.requestRemote('wechat_login/', requestInfo, cb)
  },

   //活动列表
  getActivityList: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_activity_list/', requestInfo, cb)
  },

   //获取我创建的列表
  getCreateActivityList: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_create_activity_list/', requestInfo, cb)
  },

  //获取我参加的列表
  getParticipateActivityList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_participate_activity_list/', requestInfo, cb)
  },

  //获取我参加俱乐部的列表
  getParticipateClubList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_participate_club_list/', requestInfo, cb)
  },

  //获取我的全国积分列表
  getScoreList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_score_list/', requestInfo, cb)
  },

  //获取俱乐部积分列表
  getClubScoreList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_club_score_list/', requestInfo, cb)
  },

  //获取俱乐部列表
  getClubList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_club_list/', requestInfo, cb)
  },

  //创建活动
  createActivity: function (cb, requestInfo) {
    var that = this
    that.requestRemote('create_activity/', requestInfo, cb)
  },

  //获取房间详情
  getRoomDetail: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_room_detail/', requestInfo, cb)
  },

  //报名
  createParticipate: function (cb, requestInfo) {
    var that = this
    that.requestRemote('create_participate/', requestInfo, cb)
  },

  //成为法官
  createJudge: function (cb, requestInfo) {
    var that = this
    that.requestRemote('create_judge/', requestInfo, cb)
  },

  //撤销
  revokeOperate: function(cb, requestInfo) {
    var that = this
    that.requestRemote('revoke_operate/', requestInfo, cb)
  },
  
  //法官离开座位
  judgeLeaveSeat: function(cb, requestInfo) {
    var that = this
    that.requestRemote('judge_leave_seat/', requestInfo, cb)
  },

  //分配角色
  assignRole: function (cb, requestInfo) {
    var that = this
    that.requestRemote('assign_role/', requestInfo, cb)
  },

  //变更位置
  assignLocation: function (cb, requestInfo) {
    var that = this
    that.requestRemote('assign_location/', requestInfo, cb)
  },

  //出局
  participantOut: function(cb, requestInfo) {
    var that = this
    that.requestRemote('participant_out/', requestInfo, cb)
  },

  //结束本局
  endActivity: function(cb, requestInfo) {
    var that = this
    that.requestRemote('end_activity/', requestInfo, cb)
  },

  // 再玩一局
  reCreateActivity: function (cb, requestInfo) {
    var that = this
    that.requestRemote('recreate_activity/', requestInfo, cb)
  },

  // 离开房间
  userLeaveRoom: function (cb, requestInfo) {
    var that = this
    that.requestRemote('user_leave_room/', requestInfo, cb)
  },

  // 获取房间活动列表
  getRoomActivityList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_room_activity_list/', requestInfo, cb)
  },


  // 获取房间人员列表
  getRoomUserList: function (cb, requestInfo) {
    var that = this
    that.requestRemote('get_room_user_list/', requestInfo, cb)
  },

   //搜索
  getSearchTender: function(cb, requestInfo){
    var that = this
    that.requestRemote('search/', requestInfo, cb)
  },



  //详情
  getTenderDetail: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_tender_detail_text/', requestInfo, cb)
  },

  //关注
  createFavorite: function(cb, requestInfo){
    var that = this
    that.requestRemote('create_favorite/', requestInfo, cb)
  },

  //取消关注
  deleteFavorite: function(cb, requestInfo){
    var that = this
    that.requestRemote('delete_favorite/', requestInfo, cb)
  },

   //获取我的关注列表
  getFavoriteList: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_wechat_favorite_list/', requestInfo, cb)
  },

  
  //获取我的订阅列表
  getSubscribeList: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_wechat_subscribe_list/', requestInfo, cb)
  },

   //获取热门搜索
  getHotSearchkeyList: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_hot_searchkey_list/', requestInfo, cb)
  },

   //获取中标详情
  getBidDetailText: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_bidding_detail_text/', requestInfo, cb)
  },

   //获取中标列表
  getBiddingList: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_bidding_list/', requestInfo, cb)
  },

  //创建订阅记录
  createSubscribedKey: function(cb, requestInfo){
    var that = this
    that.requestRemote('create_subscribed_key/', requestInfo, cb)
  },

    //获取个人订阅信息
  getSubscribeInfo: function(cb, requestInfo){
    var that = this
    that.requestRemote('get_subscribe_info/', requestInfo, cb)
  },
  
})