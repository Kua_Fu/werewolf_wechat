// pages/clubSort/clubSort.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultInfo: {
      'first_url': '../../images/first.png',
      'second_url': '../../images/second.png',
      'third_url': '../../images/third.png',
      'my_rank': '../../images/my_rank_logo.png',
    },
    requestInfo: {
      'token_id': '', 'start_index': 0, 'page_count': 10
    },
    topInfo: [],
    sortInfo: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取用户tokenID
    var that = this
    console.log('111', options)
    var club_id = options.club_id
    // var activity_id = '2017-10-25153131728364feb5b96de2e77e1abcf73c4996'
    var requestInfo = that.data.requestInfo
    requestInfo.club_id = club_id
    that.setData({
      requestInfo: requestInfo
    })
    var initFunction = function () {
      that.getClubScoreList()
    }
    weChatLogin.logIn(that, initFunction)
  },

  // 积分列表
  getClubScoreList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var topInfo = that.data.topInfo
    app.getClubScoreList(function (callBackInfo) {
      console.log('1111111', callBackInfo.data)
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        var _length = callBackData.score_list.length
        if(_length < 3){
          for (var i = 0; i < _length; i++) {
            topInfo.push(callBackData.score_list[i])
          }
        }
        else{
          for (var i = 0; i < 3; i++) {
            topInfo.push(callBackData.score_list[i])
          }
        }
   
        that.setData({
          scoreInfo: callBackData,
          topInfo: topInfo
        })
      }
    }, requestInfo)
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})