// pages/detail/detail.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    requestInfo: {
      'token_id': ''
    },
    defaultInfo: {
      'name': '名称',
      'state': '状态',
      'area': '地址',
      'date': '时间',
      'description': '详情',
      'room': '进入房间',
      'member': '人员',
      'sign': '../../images/sign.png'
    },
    roomInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    console.log('111', options)
    var activity_id = options.activity_id
    // var activity_id = '2017-10-25153131728364feb5b96de2e77e1abcf73c4996'
    var requestInfo = that.data.requestInfo
    requestInfo.activity_id = activity_id
    that.setData({
      requestInfo: requestInfo
    })
    var initFunction = function () {
      that.getRoomDetail()
    }
    weChatLogin.logIn(that, initFunction)
  },

  //获取活动详情
  getRoomDetail: function(){
    var that = this
    var requestInfo = that.data.requestInfo
    app.getRoomDetail(function(callBackInfo){
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        that.setData({
          roomInfo: callBackData
        })
        console.log('--', that.data.roomInfo)
      }
    },
    requestInfo)
  },

  //进入房间
  goToRoom: function(){
    var that = this
    var requestInfo = that.data.requestInfo
    var activity_id = requestInfo.activity_id
    wx.navigateTo({
      url: '../room/room?activity_id=' + activity_id,
    })
  },


  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (res) {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (res) {
    var that = this
    that.getRoomDetail()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})