// pages/sort/sort.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultInfo: {
      'first_url': '../../images/first.png',
      'second_url': '../../images/second.png',
      'third_url': '../../images/third.png',
      'my_rank': '../../images/my_rank_logo.png',
      'add_state': true,
      'no_more': '没有更多了'
    },
    requestInfo: {
      'token_id': '', 'start_index': 0, 'page_count': 10
    },
    topInfo: [],
    sortInfo: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取用户tokenID
    var that = this
    var initFunction = function () {
      console.log('0089', that.data.requestInfo)
      that.getScoreList()
    }
    weChatLogin.logIn(that, initFunction)
  },

  // 全国积分列表
  getScoreList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var topInfo = that.data.topInfo
    var defaultInfo = that.data.defaultInfo
    app.getScoreList(function (callBackInfo) {
      console.log('000', callBackInfo.data)
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        for(var i=0; i<3; i++){
          topInfo.push(callBackData.score_list[i])
        }
        var _length = callBackData.score_list.length
        if(_length < 10){
          defaultInfo.add_state = false
        }
        that.setData({
          scoreInfo: callBackData,
          topInfo: topInfo,
          defaultInfo: defaultInfo
        })
      }
    }, requestInfo)
  },


  //添加列表
  addList: function(){
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    var scoreInfo = that.data.scoreInfo
    if(defaultInfo.add_state == true){
      requestInfo.start_index = requestInfo.start_index + 10
      that.setData({
        requestInfo: requestInfo
      })
      app.getScoreList(function (callBackInfo) {
        console.log('000', callBackInfo.data)
        if (callBackInfo.status == 'SUCCESS') {
          var callBackData = callBackInfo.data.score_list
          var _length = callBackData.length
          for (var i = 0; i < _length; i++) {
            scoreInfo.score_list.push(callBackData[i])
          }
          if (_length < 10) {
            defaultInfo.add_state = false
          }
          that.setData({
            scoreInfo: scoreInfo,
            defaultInfo: defaultInfo
          })
        }
      }, requestInfo)
      console.log('111', scoreInfo)
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})