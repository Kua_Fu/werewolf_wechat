// pages/homePage/homePage.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultInfo: {
      'create_logo': '../../images/create_logo.png',
      'add_state': true,
      'no_more': '没有更多了'
    },
    requestInfo: {
      'token_id': '', 'start_index': 0, 'page_count': 10
    },
    activityInfo: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取用户tokenID
    var that = this
    var initFunction = function(){
      console.log('0089')
      that.getActivityList()
    }
    weChatLogin.logIn(that, initFunction)
  },

// 活动列表
getActivityList: function(){
  var that = this
  var requestInfo = that.data.requestInfo
  var defaultInfo = that.data.defaultInfo
  app.getActivityList(function (callBackInfo) {
    if (callBackInfo.status == 'SUCCESS') {
      var callBackData = callBackInfo.data
      var _length = callBackData.length
      if (_length < 10) {
        defaultInfo.add_state = false
      }
      that.setData({
        activityInfo: callBackData,
        defaultInfo: defaultInfo
      })
      
    }
  }, requestInfo)
},

//添加活动列表
addList: function () {
  var that = this
  var requestInfo = that.data.requestInfo
  var defaultInfo = that.data.defaultInfo
  var activityInfo = that.data.activityInfo
  if (defaultInfo.add_state == true) {
    requestInfo.start_index = requestInfo.start_index + 10
    that.setData({
      requestInfo: requestInfo
    })
    app.getActivityList(function (callBackInfo) {
      console.log('000', callBackInfo.data)
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        var _length = callBackData.length
        for (var i = 0; i < _length; i++) {
          activityInfo.push(callBackData[i])
        }
        if (_length < 10) {
          defaultInfo.add_state = false
        }
        that.setData({
          activityInfo: activityInfo,
          defaultInfo: defaultInfo
        })
      }
    }, requestInfo)
    console.log('111', activityInfo)
  }

},
  
  //前往创建界面
  createActivity: function(){
    wx.navigateTo({
      url: '../create/create',
    })
  },

  //前往详情界面
  goToActivityDetail: function(res){
    var that = this
    var activity_id = res.currentTarget.dataset.activityid
    wx.navigateTo({
      url: '../detail/detail?activity_id=' + activity_id,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    that.getActivityList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})