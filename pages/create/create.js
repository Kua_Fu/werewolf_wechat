// pages/create/create.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    requestInfo: {
      'token_id': ''
    },
    defaultInfo: {
      'map': '../../images/map.png',
      'activity_name_empty': '输入活动名称',
      'activity_name': '',
      'fee': '',//详细地址，暂时使用这个字段
      'activity_name_text': '名称',
      'activity_number_text': '人数',
      'activity_number': 12,
      'activity_time_text': '时间',
      'activity_address_text': '地址',
      'date': '开始日期',
      'time': '开始时间',
      'activity_type_text': '性质',
      'activity_club_text': '俱乐部',
      'activity_image_text': '封面',
      'activity_description_text': '注意事项',
      'club_select_text': '欢乐局',
      'club_select_state': false,
      'club_name': '请选择俱乐部',
      'address_state': false,
      'address': '',
      'club_id': null,
      'description_logo': '../../images/description.png',
      'imgpath': ''
    },
    inputInfo: {
      'input_page_state': false,
      'input_state': false,
      'placeholder': '请输入活动注意事项',
      'input_value': '',
      'input_number': 0,
      'total_number': 140
    },
    imageInfo: {
      'path': '../../images/album.png',
      'state': false,
      'file_path': ''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var initFunction = function () {
      that.getClubList()
    }
    weChatLogin.logIn(that, initFunction)

  },

  //活动地址
  chooseLocation: function(){
    var that = this
    var defaultInfo = that.data.defaultInfo
    wx.chooseLocation({
      success: function(res) {
        console.log('1111', res)
        defaultInfo.address = res.name
        defaultInfo.address_state = true
        defaultInfo.fee = res.address
        that.setData({
          defaultInfo: defaultInfo
        })
      },
      
    })
  },

  //获取俱乐部列表
  getClubList: function(){
      var that = this
      var requestInfo = that.data.requestInfo
      app.getClubList(function (callBackInfo) {
        
        if (callBackInfo.status == 'SUCCESS') {
          var callBackData = callBackInfo.data
          that.setData({
            clubInfo: callBackData
          })
        }
      }, requestInfo)
  },

  //变更活动名称
  updateActivityName: function(res){
      var that = this
      var _value = res.detail.value
      var defaultInfo = that.data.defaultInfo
      defaultInfo.activity_name = _value
      that.setData({
        defaultInfo: defaultInfo
      })
  },

  //变更活动人数
  updateActivityNumber: function(res){
    var that = this
    var defaultInfo = that.data.defaultInfo
    defaultInfo.activity_number = res.detail.value
    that.setData({
      defaultInfo: defaultInfo
    })
  },

  //变更活动时间
  updateDateInfo: function(res){
    var that = this
    var defaultInfo = that.data.defaultInfo
    var _type = res.currentTarget.dataset.type
    var _value = res.detail.value
    console.log('000', res)
    if(_type == 'date'){
      defaultInfo.date = _value
    }
    else if(_type == 'time'){
      defaultInfo.time = _value
    }
    that.setData({
      defaultInfo: defaultInfo
    })
  }, 

  //变更活动性质
  updateClubInfo: function(res){
    var that = this
    var defaultInfo = that.data.defaultInfo
    var clubState = res.detail.value
    if(clubState == false){
      defaultInfo.club_select_text = '欢乐局'
      defaultInfo.club_select_state = false
      defaultInfo.club_name = '请选择俱乐部'
      defaultInfo.club_id = ''
    }
    else{
      defaultInfo.club_select_text = '俱乐部'
      defaultInfo.club_select_state = true
    }
    that.setData({
      defaultInfo: defaultInfo
    })
  },

  //选择俱乐部
  selectClub: function(res) {
    var that = this
    var clubInfo = that.data.clubInfo
    var defaultInfo = that.data.defaultInfo
    var selectNum = Number(res.detail.value)
    defaultInfo.club_name = clubInfo[selectNum].club_name
    defaultInfo.club_id = clubInfo[selectNum].club_id
    defaultInfo.address = clubInfo[selectNum].area
    that.setData({
      defaultInfo: defaultInfo
    })
  },

  //选择图片
  chooseImage: function(){
    var that = this
    var imageInfo = that.data.imageInfo
    wx.chooseImage({
      count: 1,
      success: function(res) {
        var tempFilePaths = res.tempFilePaths
        imageInfo.state = true
        imageInfo.file_path = tempFilePaths[0]
        that.setData({
          imageInfo: imageInfo
        })
      },
    })
  },

  //编辑活动描述
  updateDescription: function(){
    var that = this
    var inputInfo = that.data.inputInfo
    inputInfo.input_page_state = true
    that.setData({
      inputInfo: inputInfo
    })
  },

  //变更活动描述
  updateInputInfo: function (res) {
    var that = this
    var _value = res.detail.value
    console.log('111', res.detail.value, res.detail.value.length)
    var inputInfo = that.data.inputInfo
    inputInfo.input_number = _value.length
    inputInfo.input_value = _value
    that.setData({
      inputInfo: inputInfo
    })
  },

  //确定活动描述
  commitInputInfo: function(){
    var that = this
    var inputInfo = that.data.inputInfo
    if (inputInfo.input_value == 0){
      inputInfo.input_state = false
    }
    else{
      inputInfo.input_state = true
    }
    inputInfo.input_page_state = false
    that.setData({
      inputInfo: inputInfo
    })

  },


  //检验创建条件是否完整
  checkActivity: function(){
    var that = this
    var defaultInfo = that.data.defaultInfo
    var imageInfo = that.data.imageInfo
    var inputInfo = that.data.inputInfo
    console.log('1111', defaultInfo)
    //1,检查活动名称
    if(defaultInfo.activity_name.length == 0){
      wx.showModal({
        title: '提示',
        content: '请输入活动名称',
        confirmColor: '#5F56C1',
        success: function (res) {}
      })
    }
    //2,检查时间
    else if(defaultInfo.date == '开始日期'){
      wx.showModal({
        title: '提示',
        content: '请填写活动日期',
        confirmColor: '#5F56C1',
        success: function (res) { }
      })
    }
    else if(defaultInfo.time == '开始时间'){
      wx.showModal({
        title: '提示',
        content: '请填写活动时间',
        confirmColor: '#5F56C1',
        success: function (res) { }
      })
    }
    //3，判断活动性质
    else if (defaultInfo.club_select_state == false && defaultInfo.address == ''){
      wx.showModal({
        title: '提示',
        content: '请填写活动地址',
        confirmColor: '#5F56C1',
        success: function (res) { }
      })
    }
    else if (defaultInfo.club_select_state == true && defaultInfo.club_id == ''){
      wx.showModal({
        title: '提示',
        content: '请选择俱乐部',
        confirmColor: '#5F56C1',
        success: function (res) { }
      })
    }
    //4，判断活动封面
    else if(imageInfo.state == false){
      wx.showModal({
        title: '提示',
        content: '请添加活动封面',
        confirmColor: '#5F56C1',
        success: function (res) { }
      })
    }
    //5, 判断活动注意事项
    else if(inputInfo.input_value == ''){
      wx.showModal({
        title: '提示',
        content: '请填写活动注意事项',
        confirmColor: '#5F56C1',
        success: function (res) { }
      })
    }
    //上传图片
    else{
      wx.showLoading({
        title: '创建中',
        mask: true
      })
      that.uploadFile()
    }



  },

  //上传图片
  uploadFile: function(res){
    var that = this
    var imageInfo = that.data.imageInfo
    var defaultInfo = that.data.defaultInfo
    console.log(imageInfo)
    wx.uploadFile({
      url: 'https://thewind.xin/werewolf/upload_file/', //仅为示例，非真实的接口地址
      filePath: imageInfo.file_path,
      name: 'file',
      formData: {
        'user': 'test'
      },
      success: function (res) {
        console.log('1111', res)
        defaultInfo.imgpath = JSON.parse(res.data)['data']
        that.setData({
          defaultInfo: defaultInfo
        })
        that.createActivity()
      }
    })

  },

  createActivity: function(){
    var that = this
    var defaultInfo = that.data.defaultInfo
    var inputInfo = that.data.inputInfo
    console.log('---', defaultInfo)
    var requestInfo = {}
    requestInfo['token_id'] = that.data.requestInfo.token_id
    requestInfo['activity_name'] = defaultInfo.activity_name
    requestInfo['activity_number'] = defaultInfo.activity_number
    requestInfo['fee'] = defaultInfo.fee
    if (defaultInfo.club_id == null){
      requestInfo['type_id'] = 1
    }
    else{
      requestInfo['type_id'] = 2
    }
    requestInfo['startdate'] = defaultInfo.date
    requestInfo['starttime'] = defaultInfo.time
    requestInfo['description'] = inputInfo.input_value
    requestInfo['club_id'] = defaultInfo.club_id
    requestInfo['area'] = defaultInfo.address
    requestInfo['imgpath'] = defaultInfo.imgpath
    app.createActivity(function (callBackInfo) {
      console.log('活动', callBackInfo)
      if (callBackInfo.status == 'SUCCESS') {
        wx.hideLoading()
        wx.switchTab({
          url: '../homePage/homePage',
        })
      }
    }, requestInfo)
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (res) {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (res) {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})