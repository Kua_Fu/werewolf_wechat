// pages/userInfo/userInfo.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultInfo: {
      'bg': '../../images/bg.png',
      'add_create_state': true,
      'add_part_state': true,
      'add_club_state': true,
      'no_more': '没有更多了'
    },
    requestInfo: {
      'token_id': '', 'start_index': 0, 'page_count': 10
    },
    userInfo: { 'avatar_url': ''},
    selectInfo: [
      { 'color': '#5F56C1', 'text': '创建', 'url': '../../images/user_create.png'},
      { 'color': '#888888', 'text': '参加', 'url': '../../images/user_participate.png'},
      { 'color': '#888888', 'text': '俱乐部', 'url': '../../images/user_club.png'},
    ],
    createInfo: [],
    participateInfo: [],
    clubInfo: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取用户tokenID
    var that = this
    var initFunction = function () {
      console.log('9999')
      that.getUserInfo()
      that.getCreateActivityList()
    }
    weChatLogin.logIn(that, initFunction)
  },


  // 获取用户信息
  getUserInfo: function(){
    var that = this
    var userInfo = that.data.userInfo
    wx.getUserInfo({
      success: function (res) {
        console.log('====',res)
        var userInfo = res.userInfo
        // var nickName = userInfo.nickName
        var avatarUrl = userInfo.avatarUrl
        // var gender = userInfo.gender //性别 0：未知、1：男、2：女
        // var province = userInfo.province
        // var city = userInfo.city
        // var country = userInfo.country
        userInfo.avatar_url = avatarUrl
        // userInfo.nickname = nickName
        that.setData({
          userInfo: userInfo
        })
        console.log('-===',userInfo)
      }
    })
  },

  //变更选项
  updateSelectInfo: function(options){
    var that = this
    var clickNum = options.currentTarget.dataset.clicknum
    console.log('----', clickNum, typeof(clickNum))
    var selectInfo = that.data.selectInfo
    for (var i=0; i< selectInfo.length; i++){
      if(i == clickNum){
        selectInfo[i].color = '#5F56C1'
      }
      else{
        selectInfo[i].color = '#888888'
      }
    }
    if(clickNum == 1){
      that.getParticipateActivityList()
    }
    else if(clickNum == 2){
      that.getParticipateClubList()
    }
    that.setData({
      selectInfo: selectInfo
    })
    console.log('---', selectInfo)
  },

  //获取我创建的活动列表
  getCreateActivityList: function(){
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    requestInfo.start_index = 0
    requestInfo.page_count = 10
    app.getCreateActivityList(function(callBackInfo) {
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        var _length = callBackData.length
        if (_length < 10) {
          defaultInfo.add_create_state = false
        }
        that.setData({
          createInfo: callBackData,
          defaultInfo: defaultInfo
        })
      }
    }, requestInfo)
  },

  //获取我参加的活动列表
  getParticipateActivityList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    requestInfo.start_index = 0
    requestInfo.page_count = 10
    app.getParticipateActivityList(function (callBackInfo) {
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        var _length = callBackData.length
        if (_length < 10) {
          defaultInfo.add_part_state = false
        }
        that.setData({
          participateInfo: callBackData,
          defaultInfo: defaultInfo
        })
      }
    }, requestInfo)
    console.log('===', defaultInfo)
  },

  //获取我参加的俱乐部列表
  getParticipateClubList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    requestInfo.start_index = 0
    requestInfo.page_count = 10
    app.getParticipateClubList(function (callBackInfo) {
      console.log('111', callBackInfo)
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        var _length = callBackData.length
        if (_length < 10) {
          defaultInfo.add_club_state = false
        }
        that.setData({
          clubInfo: callBackData,
          defaultInfo: defaultInfo
        })
      }
    }, requestInfo)
  },

  //添加活动列表
  addCreateList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    var createInfo = that.data.createInfo
    if (defaultInfo.add_create_state == true) {
      requestInfo.start_index = requestInfo.start_index + 10
      that.setData({
        requestInfo: requestInfo
      })
      app.getCreateActivityList(function (callBackInfo) {
        console.log('000', callBackInfo.data)
        if (callBackInfo.status == 'SUCCESS') {
          var callBackData = callBackInfo.data
          var _length = callBackData.length
          for (var i = 0; i < _length; i++) {
            createInfo.push(callBackData[i])
          }
          if (_length < 10) {
            defaultInfo.add_create_state = false
          }
          that.setData({
            createInfo: createInfo,
            defaultInfo: defaultInfo
          })
        }
      }, requestInfo)
      console.log('111', createInfo)
    }

  },

  //添加活动列表
  addPartList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    var participateInfo = that.data.participateInfo
    if (defaultInfo.add_part_state == true) {
      requestInfo.start_index = requestInfo.start_index + 10
      that.setData({
        requestInfo: requestInfo
      })
      app.getParticipateActivityList(function (callBackInfo) {
        console.log('000', callBackInfo.data)
        if (callBackInfo.status == 'SUCCESS') {
          var callBackData = callBackInfo.data
          var _length = callBackData.length
          for (var i = 0; i < _length; i++) {
            participateInfo.push(callBackData[i])
          }
          if (_length < 10) {
            defaultInfo.add_part_state = false
          }
          that.setData({
            participateInfo: participateInfo,
            defaultInfo: defaultInfo
          })
        }
      }, requestInfo)
      console.log('111', participateInfo)
    }

  },


  //添加活动列表
  addClubList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    var defaultInfo = that.data.defaultInfo
    var clubInfo = that.data.clubInfo
    if (defaultInfo.add_club_state == true) {
      requestInfo.start_index = requestInfo.start_index + 10
      that.setData({
        requestInfo: requestInfo
      })
      app.getParticipateClubList(function (callBackInfo) {
        console.log('000', callBackInfo.data)
        if (callBackInfo.status == 'SUCCESS') {
          var callBackData = callBackInfo.data
          var _length = callBackData.length
          for (var i = 0; i < _length; i++) {
            clubInfo.push(callBackData[i])
          }
          if (_length < 10) {
            defaultInfo.add_club_state = false
          }
          that.setData({
            clubInfo: clubInfo,
            defaultInfo: defaultInfo
          })
        }
      }, requestInfo)
      console.log('111', clubInfo)
    }

  },

  //前往详情页面
  goToDetail: function(res){
    var that = this
    var activity_id = res.currentTarget.dataset.activityid
    wx.navigateTo({
      url: '../detail/detail?activity_id=' + activity_id,
    })
  },

  //前往俱乐部页面
  goToSort: function (res) {
    var that = this
    var club_id = res.currentTarget.dataset.clubid
    wx.navigateTo({
      url: '../clubSort/clubSort?club_id=' + club_id,
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})