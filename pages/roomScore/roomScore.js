// pages/roomScore/roomScore.js
var util = require('../../utils/util.js')
var weChatLogin = require('../wechatLogin/wechatLogin.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    requestInfo: {
      'token_id': ''
    },
    defaultInfo: {
      'score': '../../images/score.png',
      'role_1': '../../images/role_1.png',
      'role_2': '../../images/role_2.png',
      'role_3': '../../images/role_3.png',
      'role_4': '../../images/role_4.png',
      'win': '../../images/win.png',
      'fail': '../../images/fail.png',
    },
    activityInfo: [],
    userList: [],
    topInfo: [
      {'text': '活动', 'state': true, 'class': 'click'},
      { 'text': '人员', 'state': false, 'class': 'unclick' },
    ]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    console.log('111', options)
    var activity_id = options.activity_id
    // var activity_id = '2017-10-26170840ae104f6dede1598f330e6f6bce3af372'
    var requestInfo = that.data.requestInfo
    requestInfo.activity_id = activity_id
    that.setData({
      requestInfo: requestInfo
    })
    var initFunction = function () {
      that.getRoomActivityList()
      that.getRoomUserList()
    }
    weChatLogin.logIn(that, initFunction)
  },

  //变更列表
  updateTopInfo: function(res){
    var that = this
    var clickNum = res.currentTarget.dataset.clicknum
    var topInfo = that.data.topInfo
    for(var i=0; i<2; i++){
      if(i == clickNum){
        topInfo[i].class = 'click'
        topInfo[i].state = true
      }
      else{
        topInfo[i].class = 'unclick'
        topInfo[i].state = false
      }
    }
    that.setData({
      topInfo: topInfo
    })
  },

  //获取房间活动列表
  getRoomActivityList: function(){
    var that = this
    var requestInfo = that.data.requestInfo
    var activityInfo = that.data.activityInfo
    app.getRoomActivityList(function(callBackInfo){
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        
        for(var i=0; i<callBackData.length; i++){
          activityInfo.push({ 
            'state': false, 'score': [],
            'activity_id': callBackData[i].activity_id, 
            'imgpath': callBackData[i].imgpath, 
            'activity_name': callBackData[i].activity_name, 
            })
        }
        that.setData({
          activityInfo: activityInfo,
        })
        console.log('---', activityInfo)
      }
    }, requestInfo)

  },

  //变更活动列表
  updateActivityInfo: function(res){
    var that = this
    var activityInfo = that.data.activityInfo
    var clickNum = res.currentTarget.dataset.clicknum
    var activity_id = res.currentTarget.dataset.activityid
    console.log('====', clickNum, activityInfo[clickNum])
    //判断是否已经点击
    if(activityInfo[clickNum].score.length == 0){
      var requestInfo = {
        'token_id': that.data.requestInfo.token_id,
        'activity_id': activity_id
      }
      app.getRoomDetail(function(callBackInfo){
        if (callBackInfo.status == 'SUCCESS') {
          var callBackData = callBackInfo.data.role_list
          console.log('111', callBackData)
          for(var i=0; i<callBackData.length; i++){
            activityInfo[clickNum].score.push(callBackData[i])
          }
          console.log('00000', activityInfo)
          that.setData({
            activityInfo: activityInfo
          })
        }
      }, requestInfo)
    }
    var _state = activityInfo[clickNum].state 
    if(_state == false){
      activityInfo[clickNum].state = true
    }
    else{
      activityInfo[clickNum].state = false
    }
    that.setData({
      activityInfo: activityInfo
    })
  },

  //获取房间人员列表
  getRoomUserList: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    app.getRoomUserList(function (callBackInfo) {
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        console.log('=====', callBackData)
        that.setData({
          userList: callBackData,
        })
      }
    }, requestInfo)
  },

  //获取活动详情
  getRoomDetail: function () {
    var that = this
    var requestInfo = that.data.requestInfo
    app.getRoomDetail(function (callBackInfo) {
      if (callBackInfo.status == 'SUCCESS') {
        var callBackData = callBackInfo.data
        console.log('+++', callBackData)
        that.setData({
          userList: userList
        })
      }
    }, requestInfo)
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function (res) {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (res) {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '狼人杀',
      path: '/pages/homePage/homePage',
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})