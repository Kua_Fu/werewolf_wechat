//模块化接口
var app = getApp()

//将请求数据转化为符合微信json格式
function json2Form(json) {  
    var str = [];  
    for(var p in json){  
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(json[p]));  
    }  
    return str.join("&");  
}


//跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
function switchTabBar(urlInfo){
    wx.switchTab({
        url: '../' + urlInfo + '/' + urlInfo
    })
}

//关闭当前页面，跳转到应用内的某个页面
function redirectToOtherPage(urlInfo){
    wx.redirectTo({
        url: '../' + urlInfo + '/' + urlInfo
    })
}

//保留当前页面，跳转到应用内的某个页面
function navigateToOtherPage(event){
  var urlInfo = event.currentTarget.dataset.url
  wx.navigateTo({
    url: '../' + urlInfo + '/' + urlInfo
  })
}

//保留当前页面，跳转到应用内的某个页面（带参数）
function navigateToOtherPageWithData(event){
  var urlInfo = event.currentTarget.dataset.url
  var title = event.currentTarget.dataset.title
  var data = event.currentTarget.dataset.data
  wx.navigateTo({
      url: '../' + urlInfo + '/' + urlInfo + '?' + title + '=' + data
  })
}

//同步设置本地缓存中的信息
function setStorageSynchronize(key, value){
  wx.setStorageSync(key, value)
}

//获取缓存中信息
function getStorageSynchronize(key){
  var value = wx.getStorageSync(key)
  return  value
}



//显示导航条加载动画
function showNavigationBarLoading(){
  wx.showNavigationBarLoading()
}

//普通的消息提示框
function showToast(title, icon, mask, duration){
    wx.showToast({
      title: title,
      icon: icon,
      mask: mask, 
      duration: duration
    })
}

//隐藏消息提示框
function hideToast(){
   wx.hideToast()
}

//隐藏导航条加载动画
function hideNavigationBarLoading(){
  wx.hideNavigationBarLoading()
}

//动态设置导航栏标题
function setNavigationBarTitle(title){
  wx.setNavigationBarTitle({
    title: title
  })
}



module.exports = {  
  json2Form:json2Form,
  switchTabBar: switchTabBar,
  redirectToOtherPage: redirectToOtherPage,
  navigateToOtherPage: navigateToOtherPage,
  navigateToOtherPageWithData: navigateToOtherPageWithData,
  setStorageSynchronize: setStorageSynchronize,
  getStorageSynchronize: getStorageSynchronize,
  showNavigationBarLoading: showNavigationBarLoading,
  hideNavigationBarLoading: hideNavigationBarLoading,
  showToast: showToast,
  hideToast: hideToast,
  setNavigationBarTitle: setNavigationBarTitle
}